//
//  MedicationCatalogVC.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 02/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class MedicationCatalogVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    // UI Components
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    // Basic Variables
    //var medications = [Medication]()
    var inSearchMode = false
    var filteredMedications = [Medication]()
    var medications = [Medication]()
    var medicationEntities = [MedicationEntity]()
    //var fetchResultController: NSFetchedResultsController!

    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self;
        tableView.delegate = self;
        tableView.dataSource = self;
        searchBar.returnKeyType = UIReturnKeyType.Done

        print("Current Set: ")
        print(medications)

    }

    override func viewDidAppear(animated: Bool) {
        if medications.count > 0 {
            print(medications)
            medications.removeAll()
        }
        fetchAndSetResults()
        tableView.reloadData()
    }

    func fetchAndSetResults() {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "MedicationEntity")

        do {
            let results = try context.executeFetchRequest(fetchRequest)
            if let medEntities = results as? [MedicationEntity] {
                for medicine in medEntities {
                    print(medicine)
                }
                for var x = 0; x < medEntities.count; x++ {
                    let med = Medication(id: medEntities[x].id as! Int)
                    print(med)
                    if medEntities[x].name!.capitalizedString != "crystal meth".capitalizedString {
                        med.AutoMapper(medEntities[x])
                        medications.append(med)
                    }
                }
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        if inSearchMode {
            return filteredMedications.count
        }

        return medications.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell =
        tableView.dequeueReusableCellWithIdentifier("medCell", forIndexPath: indexPath) as? MedicationCell {
            var meds = Medication!()
            if inSearchMode {
                meds = filteredMedications[indexPath.row]
            } else {
                meds = medications[indexPath.row]
                cell.configureCell(meds)
                return cell
            }
        }
        return MedicationCell()
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false;
            view.endEditing(true)
            tableView.reloadData()
        } else {
            inSearchMode = true;
            let lower = searchBar.text!.lowercaseString
            //let text = searchBar.text!
            filteredMedications = medications.filter({ $0.name.lowercaseString.rangeOfString(lower) != nil })
            print(filteredMedications)
            tableView.reloadData()
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        var med: Medication!

        if inSearchMode {
            med = filteredMedications[indexPath.row]
        } else {
            med = medications[indexPath.row]
        }

        print(med.name)
        print(med.description)
        performSegueWithIdentifier("MedicationDetailsVC", sender: med)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MedicationDetailsVC" {
            if let detailsVC = segue.destinationViewController as? MedicationDetailsVC {
                if let med = sender as? Medication {
                    detailsVC.medication = med;
                }
            }
        }
    }
}




























