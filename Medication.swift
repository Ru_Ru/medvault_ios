//
//  Medication.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 31/10/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation
import Alamofire

class Medication {

    private var _id: Int!
    private var _name: String!
    private var _type: String!
    private var _img: String?
    private var _description: String?
    private var _medUrl: String!
    private var _symptoms = [String]()

    var id: Int {
        get {
            return _id
        }
        set {
            if newValue > 0 && _id == nil {
                _id = newValue;
            }
        }
    }

    var name: String {
        get {
            return _name
        }
        set {
            if newValue != "" {
                _name = newValue
            }
        }
    }

    var type: String {
        get {
            return _type
        }
        set {
            if newValue != "" {
                _type = newValue
            }
        }
    }

    var img: String {
        get {
            return _img!
        }
        set {
            //if newValue != "" {
            _img = newValue
            //}
        }
    }

    var description: String {
        get {
            return _description!
        }
        set {
            _description = newValue
        }
    }

    var symptoms: [String] {
        get {
            return self._symptoms
        }
        set {
            _symptoms = newValue
        }
    }

    var medUrl: String {
        get {
            return self._medUrl!
        }
    }

    init() {

    }

    init(id: Int) {
        self._id = id;

        _medUrl = "\(URL_BASE)\(URL_MEDICATION)\(self.id)/details"
    }

    init(id: Int, name: String, type: String) {
        self._id = id;
        self._name = name;
        self._type = type;

        _medUrl = "\(URL_BASE)\(URL_MEDICATION)\(self.id)/details"
    }


    init(id: Int, name: String, type: String, img: String, description: String) {
        self._id = id;
        self._name = name;
        self._type = type;
        self._img = img;
        self._description = description

        _medUrl = "\(URL_BASE)\(URL_MEDICATION)\(self.id)/details"
    }


    init(id: Int, name: String, type: String, img: String) {
        self._id = id;
        self._name = name;
        self._type = type;
        self._img = img;

        _medUrl = "\(URL_BASE)\(URL_MEDICATION)\(self.id)/details"
    }

    // All Medications
    static func downloadAllMedications(var medications: [Medication], completed: DownloadAll) -> [Medication] {

        let url = NSURL(string: "\(URL_BASE)/\(URL_MEDICATIONS)")!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result;
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let medicationsJSON = dict["medications"] as? [Dictionary<String, AnyObject>] where medicationsJSON.count > 0 {
                    for var x = 1; x < medicationsJSON.count; x++ {
                        let medication = Medication(id: (medicationsJSON[x]["id"] as? Int)!)
                        if let medName = medicationsJSON[x]["name"] as? String {
                            medication.name = medName
                        }
                        if let medDescription = medicationsJSON[x]["description"] as? String {
                            medication.description = medDescription;
                        }
                        if let medType = medicationsJSON[x]["type"] as? String {
                            medication.type = medType;
                        }
                        //let thisMed = Medication(medName, medDescription, medType)
                        medications.append(medication)
                    }
                }
            }
        }
        print(medications)
        if medications.capacity < 1 {
            print("Emtpy, initialization failed")
        }
        if medications.capacity > 1 {

            print("Good to go array")
            return medications;
        }

        return medications;
    }

    // To Medication
    func AutoMapper(entity: MedicationEntity) {
        print(entity)
        self._name = entity.name;
        self._type = entity.type;
        self.img = entity.pic
        self.description = entity.info!
        for symp in entity.symptoms {
            let s = symp
            if let thisName = s.name {
                self._symptoms.append(thisName)
            }

            print(self)
        }
    }


    func downloadMedDetails(completed: DownloadComplete) {

        let url = NSURL(string: self.medUrl)!
        Alamofire.request(.GET, url).responseJSON {
            response in
            let result = response.result
            print("=======")
            print(result.value)
            print(result.value?.debugDescription)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let name = dict["name"] as? String {
                    self._name = name;
                }
                if let type = dict["type"] as? String {
                    self._type = type;
                }
                if let description = dict["description"] as? String {
                    self._description = description
                }

                print(self._name)
                print(self._description)
                print(self._type)

                if let symptoms = dict["symptoms"] as? [Dictionary<String, AnyObject>]  where symptoms.count > 0 {
                    if let symptom = symptoms[0]["name"] {
                        self._symptoms.append(symptom as! String)
                    }
                    if symptoms.count > 1 {
                        for var x = 1; x < symptoms.count; x++ {
                            if let symptom = symptoms[x]["name"] {
                                self._symptoms.append(symptom as! String)
                            }
                        }
                    }
                } else {
                    self._symptoms = []
                }
                print(self._symptoms)
            }
        }
    }

}
