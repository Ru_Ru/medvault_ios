//
//  UserEntity+CoreDataProperties.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 26/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserEntity {

    @NSManaged var id: NSNumber?
    @NSManaged var username: String?
    @NSManaged var name: String?
    @NSManaged var password: NSNumber?
    @NSManaged var dateOfBirth: NSDate?
    @NSManaged var prescriptions: NSSet?

}
