//
//  MedicationCatalog.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 02/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit

class MedicationCatalog: UITableViewController {

    
    var medications: [Medication] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let m1 = Medication(id: 1, name: "Paracetamol", type: "Tablets")
        let m2 = Medication(id: 2, name: "Stepsil",     type: "Sweets")
        let m3 = Medication(id: 3, name:  "KidneyKing", type:  "Capsules")
        let m4 = Medication(id: 4, name:  "Nicorette Freshmint 4mg Gum", type:  "Gum")
        let m5 = Medication(id: 5, name:  "KidneyKing", type:  "Capsules")
        let m6 = Medication(id: 6, name:  "KidneyKing", type:  "Capsules")
        let m7 = Medication(id: 7, name:  "KidneyKing", type:  "Capsules")
        
        medications.append(m1)
        medications.append(m2)
        medications.append(m3)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return medications.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell =
        tableView.dequeueReusableCellWithIdentifier("medId", forIndexPath: indexPath)

        cell.textLabel?.text = medications[indexPath.row].name
        cell.detailTextLabel?.text = medications[indexPath.row].type

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
