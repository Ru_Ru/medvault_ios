//
//  MedicationEntity.swift
//  
//
//  Created by Ruslanas Jonusas on 23/11/15.
//
//

import Foundation
import CoreData
import UIKit
import Alamofire


//@objc(MedicationEntity)

class MedicationEntity: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    var symptoms: [SymptomEntity] {
        get {
            return self.mSymptoms!.allObjects as! [SymptomEntity]
        }
        set {
            self.mSymptoms = NSSet(array: newValue)
        }
    }
    
    var pic: String {
        get {
            return self.image!
        }
        set {
            self.image = newValue
        }
    }

    func setMedicationImg(img: UIImage) {
        let data = UIImagePNGRepresentation(img)
        self.img = data
    }

    func getMedImg() -> UIImage {
        let img = UIImage(data: self.img!)
        return img!
    }

    static func downloadAllMedications(completed: DownloadComplete) {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let medEntity = NSEntityDescription.entityForName("MedicationEntity", inManagedObjectContext: context)!
        //let sympEntity = NSEntityDescription.entityForName("SymptomEntity", inManagedObjectContext: context)!
        let fetchRequest = NSFetchRequest(entityName: "SymptomEntity")

        let url = NSURL(string: "\(URL_BASE)\(URL_MEDICATIONS)")!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let medicationsJSON = dict["medications"] as? [Dictionary<String, AnyObject>] where medicationsJSON.count > 0 {
                    for var x = 0; x < medicationsJSON.count; x++ {
                        //
                        let medication = MedicationEntity(entity: medEntity, insertIntoManagedObjectContext: context)
                        let medId = x + 1
                        medication.id = medId;
                        if let medName = medicationsJSON[x]["name"] as? String {
                            medication.name = medName
                        }
                        if let medType = medicationsJSON[x]["type"] as? String {
                            medication.type = medType;
                        }
                        if let medImg = medicationsJSON[x]["img"] as? String {
                            medication.pic = medImg;
                        }
                        if let medDesc = medicationsJSON[x]["description"] as? String {
                            medication.info = medDesc;
                        }

                        if let symptoms = medicationsJSON[x]["symptoms"] as? [Dictionary<String, AnyObject>] {
                            // Go through each symptom under medication
                            for var y = 0; y < symptoms.count; y++ {
                                let symp = symptoms[y]["name"] as? String // get a name of the symptom
                                do {
                                    // Fetch all symptoms
                                    if let results = try context.executeFetchRequest(fetchRequest) as? [SymptomEntity] {
                                        //fetchRequest.predicate = NSPredicate(format: "name == %@", symp!)
                                        // Loop through fetched symptoms and find a match
                                        for symptom in results {
                                            if symptom.name == symp {
                                                medication.symptoms.append(symptom)
                                            }
                                        }
                                    }
                                } catch let error as NSError {
                                    print(error.debugDescription)
                                }
                            }
                        }

                        context.insertObject(medication)
                        do {
                            try context.save()
                        } catch {
                            print("Could not download medications")
                        }
                    }
                }
            }
        }
    }
}
