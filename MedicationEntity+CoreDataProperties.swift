//
//  MedicationEntity+CoreDataProperties.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 01/12/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MedicationEntity {

    @NSManaged var id: NSNumber?
    @NSManaged var img: NSData?
    @NSManaged var name: String?
    @NSManaged var type: String?
    @NSManaged var image: String?
    @NSManaged var info: String?
    @NSManaged var mSymptoms: NSSet?

}
