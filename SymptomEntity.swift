//
//  SymptomEntity.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 26/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation
import CoreData
import Alamofire


class SymptomEntity: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    @NSManaged var id: NSNumber?
    @NSManaged var name: String?

    static func downloadAllSymptoms(completed: DownloadComplete) {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let entity = NSEntityDescription.entityForName("SymptomEntity", inManagedObjectContext: context)!

        let url = NSURL(string: "\(URL_BASE)\(URL_SYMPTOMS)")!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result;
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let symptomsJSON = dict["symptoms"] as? [Dictionary<String, AnyObject>] where symptomsJSON.count > 0 {
                    for var x = 0; x < symptomsJSON.count; x++ {
                        let thisSymptom = SymptomEntity(entity: entity, insertIntoManagedObjectContext: context)
                        if let id = symptomsJSON[x]["id"] as? Int {
                            thisSymptom.id! = id
                        }
                        if let symptom = symptomsJSON[x]["name"] as? String {
                            thisSymptom.name = symptom
                        }
                        // persist
                        context.insertObject(thisSymptom)
                        do {
                            try context.save()
                        } catch {
                            print("Could not save and create symptoms")
                        }
                    }
                }
            }
        }
    }
}
