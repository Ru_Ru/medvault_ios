//
//  VaultViewController.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 31/10/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class VaultViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var gender: UILabel!

    var user = User()

    var prescriptions = [Prescription]()

    // UI Components
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "My Vault"

        name.text = "John Doe"
        age.text = "14/11/1989"
        gender.text = "Male"

        user.downloadUser { () -> () in

        }

        tableView.delegate = self;
        tableView.dataSource = self;

    }

    override func viewDidAppear(animated: Bool) {
        if prescriptions.count > 0 {
            print(prescriptions)
            prescriptions.removeAll()
            //tableView.reloadData()
        }
        fetchAndSetResults()
        tableView.reloadData()
    }

    func fetchAndSetResults() {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "PrescriptionEntity")

        do {
            let results = try context.executeFetchRequest(fetchRequest)
            if let entities = results as? [PrescriptionEntity] {
                for presc in entities {
                    let thisPresc = Prescription()
                    thisPresc.AutoMapper(presc)
                    for med in presc.medications {
                        let medicine = Medication(id: med.id as! Int)
                        medicine.AutoMapper(med)
                        thisPresc.medications.append(medicine)
                    }
                    prescriptions.append(thisPresc)
                }
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        return prescriptions.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell =
        tableView.dequeueReusableCellWithIdentifier("prescCell", forIndexPath: indexPath) as? PrescriptionCell {
            var presc = Prescription()
            presc = prescriptions[indexPath.row]
            cell.configureCell(presc)
            return cell

        }
        return PrescriptionCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var presc: Prescription!
        presc = prescriptions[indexPath.row]

        print(presc.title)
        performSegueWithIdentifier("PrescriptionDetailsVC", sender: presc)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PrescriptionDetailsVC" {
            if let detailsVC = segue.destinationViewController as? PrescriptionDetailsVC {
                if let presc = sender as? Prescription {
                    detailsVC.prescription = presc;
                }
            }
        }
    }
}
