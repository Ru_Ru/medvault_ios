//
//  UserEntity.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 26/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation
import CoreData
import Alamofire


class UserEntity: NSManagedObject {
    
    @NSManaged var id: NSNumber?
    @NSManaged var username: String?
    @NSManaged var name: String?
    @NSManaged var password: NSNumber?
    @NSManaged var dateOfBirth: NSDate?
    @NSManaged var prescriptions: NSSet?


    var date: NSDate {
        get {
            return self.dateOfBirth!
        }
        set {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            self.dateOfBirth = newValue
        }
    }

    convenience init(_entity: NSEntityDescription, _insertIntoManagedObjectContext: NSManagedObjectContext?) {
        self.init(entity: _entity, insertIntoManagedObjectContext: _insertIntoManagedObjectContext)
    }

//    init(_entity: NSEntityDescription, _insertIntoManagedObjectContext: NSManagedObjectContext?) {
//        self.init(entity: _entity, insertIntoManagedObjectContext: _insertIntoManagedObjectContext)
//    }

    convenience init(name: String, date: NSDate, _entity: NSEntityDescription, _insertIntoManagedObjectContext: NSManagedObjectContext?) {
        self.init(entity: _entity, insertIntoManagedObjectContext: _insertIntoManagedObjectContext)
        self.name = name
        self.dateOfBirth = date;
    }


    func hasValue() {

    }

    static func loadUser(completed: DownloadComplete) {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let userEntity = NSEntityDescription.entityForName("UserEntity", inManagedObjectContext: context)!

        let url = NSURL(string: "\(URL_BASE)\(URL_USER)")!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                    let user = UserEntity(entity: userEntity, insertIntoManagedObjectContext: context)
                    if let id = dict["id"] as? Int {
                        user.id = id
                    }
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    user.date = dateFormatter.dateFromString("12/9/1989")!
                
                    context.insertObject(user)
                    do {
                        try context.save()
                    } catch {
                        print("Could not download medications")
                    }
                }
            }
        }
}

