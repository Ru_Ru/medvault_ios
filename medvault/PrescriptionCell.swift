//
//  PrescriptionCell.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 29/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit

class PrescriptionCell: UITableViewCell {


    @IBOutlet weak var prescriptionTitle: UILabel!
    @IBOutlet weak var prescriptionStartDate: UILabel!
    @IBOutlet weak var prescriptionEndDate: UILabel!

    var prescription: Prescription!

    func configureCell(prescription: Prescription) {
        self.prescription = prescription;

        prescriptionTitle.text = self.prescription.title.capitalizedString;
        //let dateFormatter = NSDateFormatter()
        //dateFormatter.dateFormat = "dd/MM/yyyy"
        prescriptionStartDate.text = self.prescription.startDate
        prescriptionEndDate.text = self.prescription.endDate
    }

    //configureCell(name: String, )

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
