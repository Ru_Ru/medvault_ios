//
//  Constants.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 07/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation


let isRun = false
let URL_BASE = "https://mymedvault.herokuapp.com/medvault"
let URL_CATALOG = "/api/medication/search/"
let URL_MEDICATIONS = "/api/medication/all"
let URL_PRESCRIPTION = "/api/prescription/all"
let URL_MEDICATION = "/api/medication/"
let URL_SYMPTOMS = "/api/symptoms/all"
let URL_USER = "/api/user/self"

typealias DownloadComplete = () -> ()
typealias DownloadAll = () -> [Medication]