//
//  MedicationCell.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 02/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit

class MedicationCell: UITableViewCell {
    
    @IBOutlet weak var medImg: UIImageView!
    @IBOutlet weak var medName: UILabel!
    @IBOutlet weak var medSymptom: UILabel!
    @IBOutlet weak var medType: UILabel!
    
    var medication: Medication!
    //var medication: MedicationEntity!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    func configureCell(medication: Medication) {
        self.medication = medication;
        
        medImg.image = UIImage(named: "\(self.medication.img)")
        medName.text = self.medication.name.capitalizedString;
        medType.text = self.medication.type.capitalizedString;
    }
    
//    func configureCell(medication: Medication) {
//        self.medication = medication;
//        
//        medImg.image = medication.img
//        medName.text = self.medication.name.capitalizedString;
//        medType.text = self.medication.type.capitalizedString;
//    }
    
    
    func configureCell(name: String, type: String ) {
        medName.text = name;
        medType.text = type;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
