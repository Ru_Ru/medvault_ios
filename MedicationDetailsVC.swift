//
//  MedicationDetailsVC.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 15/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit
import MessageUI

class MedicationDetailsVC: UIViewController, MFMailComposeViewControllerDelegate {

    var medication: Medication!

    @IBOutlet weak var mailBtn: UIBarButtonItem!
    @IBOutlet weak var medName: UILabel!
    @IBOutlet weak var symptomLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var symptoms: UILabel!
    //@IBOutlet weak var medName: UINavigationItem!
    @IBOutlet weak var titleNI: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        //var viewSymptoms: String
        //let separator = " / "


        navigationItem.title = "Medication Overview"

        descriptionLbl.text = medication.description
        print(medication)
        typeLbl.text = medication.type
        medName.text = medication.name;
        symptoms.text = medication.symptoms.joinWithSeparator(" / ")
        //symptoms.text = medication.symptoms.join`
        mainImg.image = UIImage(named: "\(medication.img)")


        medication.downloadMedDetails {
            () -> () in

        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    @IBAction func sendEmailButtonTapped(sender: AnyObject) {
//        let mailComposeViewController = configuredMailComposeViewController()
//        if MFMailComposeViewController.canSendMail() {
//            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
//        } else {
//            print("Error Happened!")
//        }
//    }
//
//    func configuredMailComposeViewController() -> MFMailComposeViewController {
//        if (MFMailComposeViewController.canSendMail()) {
//            print("Can send email.")
//
//            let mailComposerVC = MFMailComposeViewController()
//            mailComposerVC.mailComposeDelegate = self
//
////            var symptoms = ""
////
////            for symp in medication.symptoms {
////                let symptom = symp as! String
////
////                symptoms    = symptoms + "<p>\(ingredient.amount!) - \(ingredient.ingredient!)</p>\n"
////
////            }
//
//
//            mailComposerVC.setToRecipients(["nurdin@gmail.com"])
//            mailComposerVC.setSubject("Sending The Medication")
//
//            mailComposerVC.setMessageBody(
//            "<html>" +
//                    "<body>" +
//                    "<h2>\(medication.name)</h2>" +
//                    "<p>Type:</p>" + medication.type +
//                    "<p>\(medication.description)</p>" +
//                    "</body>" +
//                    "</html>", isHTML: true)
//            return mailComposerVC
//
//            if let fileData = NSData(base64EncodedString: medication.img, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters) {
//                print("File data loaded.")
//                mailComposerVC.addAttachmentData(fileData, mimeType: "image/png", fileName: "MedPicture")
//            }
//
//            self.presentViewController(mailComposerVC, animated: true, completion: nil)
//        }
//    }

}
