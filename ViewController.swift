//
//  ViewController.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 31/10/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        navigationItem.title = "MedVault"
        
        if let Runned = NSUserDefaults.standardUserDefaults().valueForKey("Run") as? Bool {
            if Runned == true {
                print("Runned");
            }
        } else {
            loadMedicationsData();
            loadUserData();
        }
    }

    func loadMedicationsData() {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchMeds = NSFetchRequest(entityName: "MedicationEntity")
        let fetchSymps = NSFetchRequest(entityName: "SymptomEntity")
        //let deleteMeds = NSBatchDeleteRequest(fetchRequest: fetchMeds)
        //let deleteSymps = NSBatchDeleteRequest(fetchRequest: fetchSymps)
        //let deletePesct = NSBatchDeleteRequest(fetchRequest: fetchPresc)

        do {
            if let meds = try context.executeFetchRequest(fetchMeds) as? [MedicationEntity] {
                if meds.count > 0 {
                    for med in meds {
                        context.deleteObject(med)
                    }
                }
            }
            if let symps = try context.executeFetchRequest(fetchSymps) as? [SymptomEntity] {
                if symps.count > 0 {
                    for symp in symps {
                        context.deleteObject(symp)
                    }
                }
            }
        } catch let error as NSError {
            print(error.debugDescription)
        }

        SymptomEntity.downloadAllSymptoms({ () -> () in });
        MedicationEntity.downloadAllMedications({ () -> () in });
    }

    func loadUserData() {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchPresc = NSFetchRequest(entityName: "PrescriptionEntity")
//        let fetchUser = NSFetchRequest(entityName: "UserEntity")

        do {
            if let prescs = try context.executeFetchRequest(fetchPresc) as? [PrescriptionEntity] {
                if prescs.count > 0 {
                    for presc in prescs {
                        context.deleteObject(presc)
                    }
                }
            }
//            if let thisUser = try context.executeFetchRequest(fetchUser) as? UserEntity {
//                if thisUser.name != nil || thisUser.name != "" {
//                    context.deleteObject(thisUser)
//                }
//            }
        } catch let error as NSError {
            print(error.debugDescription)
        }

        PrescriptionEntity.downloadPrescriptions({ () -> () in })
//      UserEntity.loadUser({ () -> () in });
        NSUserDefaults.standardUserDefaults().setValue("true", forKey: "Runned");
        NSUserDefaults.standardUserDefaults().synchronize();
    }
}

