//
//  PrescriptionDetailsVC.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 30/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import UIKit

class PrescriptionDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var prescription: Prescription!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var prescriptionName: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    var medications = [Medication]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Prescription Overview"
        prescriptionName.text = prescription.title
        descriptionLbl.text = prescription.description
        
        for med in prescription.getMedications() {
            var medication = Medication!()
            medication = med
            medications.append(medication)
        }

        tableView.delegate = self;
        tableView.dataSource = self;

    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return medications.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell =
        tableView.dequeueReusableCellWithIdentifier("medCell", forIndexPath: indexPath) as? MedicationCell {
            var meds = Medication!()
            meds = medications[indexPath.row]
            cell.configureCell(meds)
            return cell
        }
        return MedicationCell()
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        var med: Medication!
        med = medications[indexPath.row]


        print(med.name)
        print(med.description)
        performSegueWithIdentifier("MedicationDetailsVC", sender: med)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MedicationDetailsVC" {
            if let detailsVC = segue.destinationViewController as? MedicationDetailsVC {
                if let med = sender as? Medication {
                    detailsVC.medication = med;
                }
            }
        }
    }
}
