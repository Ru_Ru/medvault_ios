//
//  PrescriptionEntity.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 26/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import Alamofire


class PrescriptionEntity: NSManagedObject {

    @NSManaged private var mId: NSNumber?
    @NSManaged private var mName: String?
    @NSManaged private var mInfo: String!
    @NSManaged private var mStartDate: NSDate?
    @NSManaged private var mEndDate: NSDate?
    @NSManaged private var mMedications: NSSet?
    @NSManaged private var mUser: UserEntity?
    //private var _prescUrl: String!

    var id: Int {
        get {
            return self.mId as! Int
        }
        set {
            self.mId = newValue
        }
    }

    var name: String {
        get {
            return self.mName!
        }
        set {
            mName = newValue
        }
    }

    var info: String {
        get {
            return self.mInfo
        }
        set {
            self.mInfo = newValue
        }
    }

    var startDate: NSDate {
        get {
            return self.mStartDate!
        }
        set {
            self.mStartDate = newValue
        }
    }

    var endDate: NSDate {
        get {
            return self.mEndDate!
        }
        set {
            self.mEndDate = newValue
        }
    }

    var medications: [MedicationEntity] {
        get {
            return self.mMedications!.allObjects as! [MedicationEntity]
        }
        set {
            self.mMedications = NSSet(array: newValue)
        }
    }

    var user: UserEntity {
        get {
            return self.mUser!
        }
        set {
            self.mUser = newValue
        }
    }

    convenience init(_entity: NSEntityDescription, _insertIntoManagedObjectContext: NSManagedObjectContext?) {
        self.init(entity: _entity, insertIntoManagedObjectContext: _insertIntoManagedObjectContext)
    }

    convenience init(id: Int, name: String, info: String, startDate: NSDate, endDate: NSDate, medications:
            NSSet, user: UserEntity) {
        self.init()
        self.mId = id;
        self.mName = name;
        self.mInfo = info;
        self.mStartDate = startDate;
        self.mEndDate = endDate;
        self.mUser = user
        self.mMedications = medications as NSSet

        //_prescUrl = "\(URL_BASE)\(URL_PRESCRIPTION)\(self.id)/details"

    }

    convenience init(id: Int, name: String, info: String, startDate: NSDate, endDate: NSDate) {
        self.init()

        self.mId = id;
        self.mName = name;
        self.mInfo = info;
        self.mStartDate = startDate;
        self.mEndDate = endDate;

        //_prescUrl = "\(URL_BASE)\(URL_PRESCRIPTION)\(self.id)/details"

    }

    convenience init(id: Int, name: String, info: String) {
        self.init()
        self.mId = id;
        self.mName = name;
        self.mInfo = info;

        // _prescUrl = "\(URL_BASE)\(URL_PRESCRIPTION)\(self.id)/details"
    }

    static func downloadPrescriptions(completed: DownloadComplete) {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let prescriptionEntity = NSEntityDescription.entityForName("PrescriptionEntity", inManagedObjectContext: context)!
        let fetchRequest = NSFetchRequest(entityName: "MedicationEntity")
        //let fetchUser = NSFetchRequest(entityName: "UserEntity")

        let url = NSURL(string: "\(URL_BASE)\(URL_PRESCRIPTION)")!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let prescriptionsJSON = dict["prescriptions"] as? [Dictionary<String, AnyObject>] where prescriptionsJSON.count > 0 {
                    for var x = 0; x < prescriptionsJSON.count; x++ {
                        let prescription = PrescriptionEntity(_entity: prescriptionEntity, _insertIntoManagedObjectContext: context)
                        let i = x + 1;
                        let prescId = i;
                        prescription.id = prescId
                        if let prescName = prescriptionsJSON[x]["name"] as? String {
                            prescription.name = prescName
                        }

                        let dateFormatter = NSDateFormatter()
                        //var date = NSLocale.
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        prescription.startDate = dateFormatter.dateFromString("27/11/2015")!
                        prescription.endDate = dateFormatter.dateFromString("19/11/2015")!

                        if let _medications = prescriptionsJSON[x]["medications"] as? [Dictionary<String, AnyObject>] {
                            for var y = 0; y < _medications.count; y++ {
                                let med = _medications[y]["name"] as? String
                                do {
                                    if let results = try context.executeFetchRequest(fetchRequest) as? [MedicationEntity] {
                                        for medication in results {
                                            if medication.name == med {
                                                prescription.medications.append(medication)
                                            }
                                        }
                                    }
                                } catch let error as NSError {
                                    print(error.debugDescription)
                                }
                            }
                        }

                        context.insertObject(prescription)
                        do {
                            try context.save()
                        } catch let error as NSError {
                            print("Count not download data: " + error.debugDescription)
                        }
                    }
                }
            }
        }
    }
}


















