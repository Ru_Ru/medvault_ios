//
// Created by Ruslanas Jonusas on 30/11/15.
// Copyright (c) 2015 Ruslanas Jonusas. All rights reserved.
//

import Foundation
import Alamofire

class User {


    private var _name: String!
    private var _dateOfBirth: String?
    private var _gender: String!
    private var _url: String!

    var name: String {
        get {
            return self._name
        }
        set {

            self._name = newValue
        }
    }
    var dateOfBirth: String {
        get {
            return self._dateOfBirth!
        }
        set {
            self._dateOfBirth = newValue
        }
    }
    var gender: String {
        get {
            return self._gender
        }
        set {
            if(newValue == "") {
                self._gender = newValue
            }
            else {
                self._gender = newValue
            }
        }
    }

    var selfUrl: String {
        get {
            return self._url
        }
        set {
            self._url = newValue
        }
    }

    init() {
        _url = "\(URL_BASE)\(URL_USER)"
    }

    init(name: String, date: String) {
        self._name = name
        self._dateOfBirth = date

        _url = "\(URL_BASE)\(URL_USER)"
    }

    init(name: String, date: String, gender: String) {
        self._name = name
        self._dateOfBirth = date
        self._gender = gender

        _url = "\(URL_BASE)\(URL_USER)"
    }

    func setUser() {
        self.name = "John Doe"
        self.dateOfBirth = "6/9/1989"
        self.gender = "Male"

        _url = "\(URL_BASE)\(URL_USER)"
    }

    func downloadUser(downloadComplete: DownloadComplete) {
        let url = NSURL(string: self.selfUrl)!

        Alamofire.request(.GET, url).responseJSON {
            response in

            let result = response.result
            print(result.value)

            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let userName = dict["name"] as? String {
                    self.name = userName
                }
                if let date = dict["birthday"] as? Dictionary<String, AnyObject> {
                    print(date)
                    let year = date["year"] as? String
                    print(year)
                }
                //let dateFormatter = NSDateFormatter()
                //dateFormatter.dateFormat = "dd/MM/yyyy"
                //let dateParse = dateFormatter.dateFromString("12/9/1989")!
                //user.dateOfBirth = dateFormatter.stringFromDate(dateParse)!

            }
        }
    }
}
