//
//  Prescription.swift
//  medvault
//
//  Created by Ruslanas Jonusas on 02/11/15.
//  Copyright © 2015 Ruslanas Jonusas. All rights reserved.

import Foundation

class Prescription {

    private var _id: Int!
    private var _title: String!
    private var _info: String!
    private var _startDate: String!
    private var _endDate: String!
    private var _medications = [Medication]()

    var title: String {
        get {
            return self._title

        }
        set {
            self._title = newValue
        }
    }

    var id: Int {
        get {
            return self._id
        }
        set {
            self._id = newValue
        }
    }

    var description: String {
        get {
            return self._info
        }
        set {
            self._info = newValue
        }
    }

    var startDate: String {
        get {
            return self._startDate
        }
        set {
            self._startDate = newValue
        }
    }

    var endDate: String {
        get {
            return self._endDate
        }
        set {
            self._endDate = newValue
        }
    }

    var medications: [Medication] {
        get {
            return self._medications
        }
        set {
            self._medications = newValue
        }
    }

    func getMedications() -> [Medication] {
        return self._medications
    }

    func setMedicatons(med: Medication) {
       self._medications.append(med)
    }

    init() {
        description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }

    init(id: Int, title: String) {
        self._title = title;
        self._id = id;

        self.description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

        //_medUrl = "\(URL_BASE)\(url)\(self.id)/details"
    }

    init(id: Int, title: String, medications: [Medication]) {
        self._title = title;
        self._id = id;
        self._medications = medications

        self.description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

        //_medUrl = "\(URL_BASE)\(url)\(self.id)/details"
    }

    func AutoMapper(entity: PrescriptionEntity) {
        self.id = entity.id
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.title = entity.name
        //self.description = entity.info
        self.startDate = dateFormatter.stringFromDate(entity.startDate)
        self.endDate = dateFormatter.stringFromDate(entity.endDate)
    }
}
